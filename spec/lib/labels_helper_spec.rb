# frozen_string_literal: true

require 'spec_helper'
require 'active_support/core_ext/numeric/time'
require_relative '../../lib/labels_helper'

RSpec.describe LabelsHelper do
  let(:resource_klass) do
    Class.new do
      include LabelsHelper

      def labels_with_details
      end
    end
  end

  let(:label_klass) do
    Struct.new(:name, :added_at)
  end

  let(:labels) { [] }

  subject { resource_klass.new }

  before do
    allow(subject).to receive(:labels_with_details).and_return(labels)
  end

  describe '#label_added_at' do
    context 'when resource has no labels' do
      it 'returns nil' do
        expect(subject.label_added_at('foo')).to be_nil
      end
    end

    context 'when resource has labels but not the given one' do
      let(:labels) { [label_klass.new('bar', Time.now.utc)] }

      it 'returns nil' do
        expect(subject.label_added_at('foo')).to be_nil
      end
    end

    context 'when resource has the given label set' do
      let(:added_at) { Time.now }
      let(:labels) { [label_klass.new('foo', added_at)] }

      it 'returns a time' do
        expect(subject.label_added_at('foo')).to eq(added_at)
      end
    end
  end

  describe '#label_added_before?' do
    context 'when resource has no labels' do
      it 'returns false' do
        expect(subject.label_added_before?('foo', Time.now.utc)).to eq(false)
      end
    end

    context 'when resource has labels but not the given one' do
      let(:labels) { [label_klass.new('bar', Time.now.utc)] }

      it 'returns false' do
        expect(subject.label_added_before?('foo', Time.now)).to eq(false)
      end
    end

    context 'when resource has the given label set' do
      let(:labels) { [label_klass.new('foo', added_at)] }

      context 'when label was not set before the given time' do
        let(:added_at) { Time.now }

        it 'returns false' do
          expect(subject.label_added_before?('foo', Time.now.utc - 2.weeks)).to eq(false)
        end
      end

      context 'when label was set before the given time' do
        let(:added_at) { 3.weeks.ago }

        it 'returns true' do
          expect(subject.label_added_before?('foo', Time.now.utc - 2.weeks)).to eq(true)
        end
      end
    end
  end
end
